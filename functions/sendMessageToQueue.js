const amqp = require('amqplib/callback_api');

exports.handler = function (event, context, callback) {
    amqp.connect(process.env.RABBITMQ_AMQP, function (error0, connection) {
        if (error0) {
            throw error0;
        }
        connection.createChannel(function (error1, channel) {
            if (error1) {
                throw error1;
            }
            const queue = 'queueAsync';
            const sendMsg = '{search request}';

            channel.assertQueue(queue, {durable: false});

            channel.sendToQueue(queue, Buffer.from(sendMsg));
            callback(null, {statusCode: 200, body: ` [x] Sent ${sendMsg}`});
        });

        setTimeout(function () {
            connection.close();
            process.exit(0)
        }, 500);
    });
};