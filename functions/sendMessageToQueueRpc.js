const amqp = require('amqplib/callback_api');

exports.handler = function (event, context, callback) {
    amqp.connect(process.env.RABBITMQ_AMQP, (error0, connection) => {
        if (error0) {
            throw error0;
        }

        connection.createChannel((error1, channel) => {
            if (error1) {
                throw error1;
            }
            channel.assertQueue('', {exclusive: true}, (error2, q) => {
                if (error2) {
                    throw error2;
                }
                const correlationId = Math.random().toString() + Math.random().toString() + Math.random().toString();

                let responses = []
                channel.consume(q.queue, (msg) => {
                    if (msg.properties.correlationId === correlationId) {
                        responses.push(msg.content)
                    }
                }, {noAck: true});

                setTimeout(() => {
                    callback(null, {statusCode: 200, body: ` [x] Sent ${sendMsg} Got ${responses.join(',')}`});
                    connection.close();
                    process.exit(0)
                }, 1500);

                const queue = 'queueSync';
                const sendMsg = '{search request}';

                let options = {
                    correlationId: correlationId,
                    replyTo: q.queue
                };
                let content = Buffer.from(sendMsg);
                channel.sendToQueue(queue, content, options);
            });
        });
    });
}