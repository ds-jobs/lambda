### Update env.json

```
{
  "sendMessageToQueue": {
    "RABBITMQ_AMQP": "amqp://192.168.42.35:5672"
  },
  "sendMessageToQueueRpc": {
    "RABBITMQ_AMQP": "amqp://192.168.42.35:5672"
  }
}
```

### Start lambdas

`sam local start-api --env-vars env.json`